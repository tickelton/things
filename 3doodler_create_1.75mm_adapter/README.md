3Doodler Create adaptor for 1.75mm Filament
===========================================

![v3_installed.jpg](https://gitlab.com/tickelton/things/raw/master/3doodler_create_1.75mm_adapter/v3_installed.jpg)

Adapter to make the 3Doodler Create work with 1.75mm Filament.

Print in ABS with 100% infill and 0.2mm resolution.

Replaces the original service hatch which makes cleaning and unclogging a
little more difficult but in turn greatly improves reliability and
reduces cost by making the pen work with standard 1.75 spool filament.

V3 is the version of the adapter that works most reliably.
V2 doesn't extend over the service slot and therefore makes it easier to
service the pen but does not work as well as V3 since there is not as
much contact between the filament and the extruder screw as with the
longer model.

